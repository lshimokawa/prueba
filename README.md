* Crear directorio
* Entrar al directorio
* $ git init
* Abrir Sublime en el directorio
* Crear un archivo README.md
* $ git status
* $ git add README.md
* $ git log
* $ git diff
* $ git checkout README.md

* $ git checkout -b branch1
* Hacer varios commits sobre branch1
* $ git checkout master
* $ git merge branch1 # Se hace con Gitlab

Partir del master
Crear 1 branch
Agregar 2 cambios y hacer commit
Regresar master
Crear otro branch
Agregar 2 cambios (sobre los mismos archivos) y hacer commit
Regresar a master
Merge a branch1
Intentar hacer merge de branch2
